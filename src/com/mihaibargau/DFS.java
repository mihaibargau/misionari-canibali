package com.mihaibargau;

import java.util.List;

public class DFS {

    public Stare executa(Stare stareInitiala) {
        System.out.println("###DFS###");
        int limita = 20;
        return DFS(stareInitiala, limita);
    }

    private Stare DFS(Stare stare, int limita) {
        if (stare.stareFinala()) {
            System.out.println("stari vizitate (valide) = " + Stare.stariVizitate);
            return stare;
        } else if (limita == 0) {

            return null;
        } else {
            List<Stare> succesori = stare.genereazaSuccesori();
            for (Stare copil : succesori) {
                Stare rez = DFS(copil, limita - 1);

                if (rez != null) {

                    return rez;
                }
            }
            return null;
        }
    }
}