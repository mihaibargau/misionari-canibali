package com.mihaibargau;

public class Main {

    public static void main(String[] args) {
        Stare stareInitiala = new Stare(3, 3, 1);

        BFS bfs = new BFS();
        Stare.solutie(bfs.executa(stareInitiala));
        DFS dfs = new DFS();
        Stare.solutie(dfs.executa(stareInitiala));

    }

}
