package com.mihaibargau;


/*
(3,3,1) partea stanga
1 - barca pe stanga
 */

import java.util.ArrayList;
import java.util.List;

public class Stare {

    private int canibali;
    private int misionari;
    private int barca;
    public static int stariVizitate = 1;
    private Stare stareParinte;


    public Stare(int canibali, int misionari, int barca) {
        this.canibali = canibali;
        this.misionari = misionari;
        this.barca = barca;
    }
    public boolean stareFinala(){
        return canibali == 0 && misionari == 0;
    }
    public boolean esteValida() {
        if (maiMultiCanibaliPeDreapta() || maiMultiCanibaliPeStanga())
            return false;
        return misionari >= 0 && canibali >= 0;
    }
    public boolean maiMultiCanibaliPeStanga() {
        return (misionari < canibali);
    }
    public boolean maiMultiCanibaliPeDreapta() {
        return ((misionari == 2 && canibali == 1) ||
                (misionari == 2 && canibali == 0) ||
                (misionari == 1 && canibali == 0));
    }
    public List<Stare> genereazaSuccesori(){
        List<Stare> succesori = new ArrayList<>();
        if (barca == 1) {
            testeazaSiAdauga(succesori, new Stare(canibali, misionari-2,0)); // 2 misionari se duc pe malul drept
            testeazaSiAdauga(succesori, new Stare(canibali-2,misionari,0)); // 2 canibali se duc pe malul drept
            testeazaSiAdauga(succesori, new Stare(canibali-1,misionari-1,0)); //1C 1M
            testeazaSiAdauga(succesori, new Stare(canibali,misionari-1,0)); //1M
            testeazaSiAdauga(succesori, new Stare(canibali-1,misionari,0)); //1C
        }
        else {
            testeazaSiAdauga(succesori, new Stare(canibali,misionari-2,1)); // 2 misionari se duc pe malul stang
            testeazaSiAdauga(succesori, new Stare(canibali-2,misionari,1)); // 2 canibali se duc pe malul stang
            testeazaSiAdauga(succesori, new Stare(canibali-1,misionari-1,1)); //1C 1M
            testeazaSiAdauga(succesori, new Stare(canibali,misionari-1,1)); //1M
            testeazaSiAdauga(succesori, new Stare(canibali-1,misionari,1)); //1C
        }

        return succesori;

    }

    private void testeazaSiAdauga(List<Stare> succesori, Stare nouaStare) {
        if (nouaStare.esteValida()) {
            ++stariVizitate;
            nouaStare.setStareParinte(this);
            succesori.add(nouaStare);
        }

    }

    public static void solutie(Stare solutie) {
        if (solutie == null) {
            System.out.print("\nFara solutie.");
        } else {
            List<Stare> lista = new ArrayList<>();
            Stare stare = solutie;
            while(stare!=null) {
                lista.add(stare);
                stare = stare.getStareParinte();
            }

            int adancime = lista.size() - 1;
            for (int i = adancime; i >= 0; i--) {
                stare = lista.get(i);
                if (stare.stareFinala()) {
                    System.out.print(stare.toString());
                } else {
                    System.out.print(stare.toString() + " -> ");
                }
            }
            System.out.println("\nadancimea la care s-a gasit solutia: " + adancime);
        }
    }


    public int getCanibali() {
        return canibali;
    }

    public void setCanibali(int canibali) {
        this.canibali = canibali;
    }

    public int getMisionari() {
        return misionari;
    }

    public void setMisionari(int misionari) {
        this.misionari = misionari;
    }

    public int getBarca() {
        return barca;
    }

    public void setBarca(int barca) {
        this.barca = barca;
    }

    public Stare getStareParinte() {
        return stareParinte;
    }

    public void setStareParinte(Stare stareParinte) {
        this.stareParinte = stareParinte;
    }

    @Override
    public String toString() {
        String pozitie;
        if (barca == 0) {
            pozitie = "DREAPTA";
        }
        else pozitie = "STANGA";
        return "Stare(" +
                "misionari=" + misionari +
                ", canibali=" + canibali +
                ", barca=" + pozitie +
                ")\n" ;
    }
}
