package com.mihaibargau;
import java.util.*;

public class BFS {

    public Stare executa(Stare stareInitiala) {
        System.out.println("###BFS###");
        if (stareInitiala.stareFinala()) {

            return stareInitiala;
        }
        Queue<Stare> coada = new LinkedList<>(); //FIFO
        Set<Stare> vizitat = new HashSet<>();
        coada.add(stareInitiala);
        while (true) {
            if (coada.isEmpty()){
                return null;
            }
            Stare stare = coada.poll();
            vizitat.add(stare);
            List<Stare> succesori = stare.genereazaSuccesori();

            for (Stare copil : succesori) {
                if (!vizitat.contains(copil) || !coada.contains(copil)){

                    if (copil.stareFinala()) {
                        System.out.println("stari vizitate (valide) = " + Stare.stariVizitate);


                        return copil;
                    }
                    coada.add(copil);
                }
            }
        }
    }
}
